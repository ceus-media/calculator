<?php
require_once 'classes/TerminalExpression.php';
require_once 'classes/Stack.php';
require_once 'classes/Number.php';
require_once 'classes/Operator.php';
require_once 'classes/Addition.php';
require_once 'classes/Subtraction.php';
require_once 'classes/Multiplication.php';
require_once 'classes/Division.php';
require_once 'classes/Power.php';
require_once 'classes/Parenthesis.php';
require_once 'classes/Math.php';

$status		= "void";
$method		= getEnv( 'REQUEST_METHOD' );
$referer	= getEnv( 'HTTP_REFERER' );

try{
	if( getEnv( 'REQUEST_METHOD' ) !== "POST" )
		throw new Exception( 'Only POST requests allowed' );
	if( !isset( $_POST['formula'] ) )
		throw new Exception( 'Formula missing' );
	$math		= new Math();
	$status		= "success";
	$formula	= str_replace( ",", ".", $_POST['formula'] );
	$answer		= $math->evaluate( $formula );
}
catch( Exception $e ){
	$status	= "error";
	$answer	= $e->getMessage();
}
print json_encode( array(
	'status'	=> $status,
	'data'		=> $answer,
	'referer'	=> $referer,
) );
