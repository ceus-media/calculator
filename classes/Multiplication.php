<?php
class Multiplication extends Operator {

    protected $precidence = 5;

    public function operate(Stack $stack) {
        $left	= $stack->pop();
		if(!$left)
			throw new Exception( 'Missing multiplicand' );
		$left	= $left->operate($stack);

        $right	= $stack->pop();
		if(!$right)
			throw new Exception( 'Missing multiplier' );
		$right	= $right->operate($stack);

        return $left * $right;
    }

}
?>
