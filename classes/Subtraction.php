<?php
class Subtraction extends Operator {

    protected $precidence = 4;

    public function operate(Stack $stack) {
        $left	= $stack->pop();
		if(!$left)
			throw new Exception( 'Missing minuend' );
		$left	= $left->operate($stack);

        $right	= $stack->pop();
		if(!$right)
			throw new Exception( 'Missing subtrahend' );
		$right	= $right->operate($stack);

        return $right - $left;
    }
}
?>
